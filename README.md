# 1. Erste Schritte

In diesem Leitfaden werden die Grundlagen von Git in Zusammenarbeit mit GitLab erläutert, sowie die Installation und Struktur des 
Gits für das Labor für Optische Diagnosemethoden und Erneuerbare Energien (ODEE) dargestellt.

Dieser Guide beinhaltet nur die Grundlagen. Es gibt massig ausführlichere Dokumentationen, wie z. B.:
* [Offizielle Git Dokumentation](https://git-scm.com/doc)
* [Offizielle GitLab Dokumentation](https://docs.gitlab.com/)

## Was ist Git & GitLab?

Git ist ein verteiltes Versionierungssystem welches frei als Open-Source zur Verfügung gestellt wird.
Es dient dazu Änderungen an Dateien (meist textbasierte Files) zu überwachen, sie rückgängig zu machen, sie anderen
über sogenannte **Repositories** (Repos) zur Verfügung zu stellen oder Aktualisierungen von anderen Mitnutzern einzuholen.

Änderungen an Dateien in einem Repo werden in **Git** durch **commits** zunächst **lokal** gespeichert wo alle Dateien inklusive deren vorangegangenen Versionen gespeichert werden.
Über einen **push** können die lokal gespeicherten Daten in ein öffentlich zugängliches (**remote**) Repo übertragen werten - dies ist hier **GitLab**.
Arbeiten mehrere Nutzer an einem Repo kann die aktuellste Version im GitLab durch einen **pull** heruntergeladen werden, um lokal daran weiterzuarbeiten.

Die Bedienung von Git erfolgt entweder über die Git-Kommandozeile (Bash) oder ein GUI (Graphical User Interface) und lässt sich auch in Programme, wie z. B. Matlab integrieren.

## Inhalt

1. [**Erste Schritte**](https://gitlab.com/odeegit/odee-gitlab-leitfaden#1-erste-schritte)
2. [**Installation**](https://gitlab.com/odeegit/odee-gitlab-leitfaden#2-installation)

	2.1 [GitLab Account erstellen](https://gitlab.com/odeegit/odee-gitlab-leitfaden#21-gitlab-account-erstellen)
	
	2.2 [Installation von Git](https://gitlab.com/odeegit/odee-gitlab-leitfaden#22-installation-von-git)
	
3. [**Bedienung**](https://gitlab.com/odeegit/odee-gitlab-leitfaden#3-bedienung)

	3.1 [Codes "nur" verwenden](https://gitlab.com/odeegit/odee-gitlab-leitfaden#31-codes-nur-verwenden)
	
	3.2 [Allgemeine Begriffe](https://gitlab.com/odeegit/odee-gitlab-leitfaden#32-allgemeine-begriffe)
	
	3.3 [Ein existierendes Repository klonen & benutzen](https://gitlab.com/odeegit/odee-gitlab-leitfaden#33-ein-existierendes-repository-klonen-benutzen-mit-git-gui)
	
	3.3.1 [Repository klonen](https://gitlab.com/odeegit/odee-gitlab-leitfaden#331-repository-klonen)
		
	3.3.2 [Repository benutzen](https://gitlab.com/odeegit/odee-gitlab-leitfaden#332-repository-benutzen)
	
	3.4 [Ein neues Repository erstellen](https://gitlab.com/odeegit/odee-gitlab-leitfaden#34-ein-neues-repository-erstellen)
	
	3.5 [Arbeiten mit Branches](https://gitlab.com/odeegit/odee-gitlab-leitfaden#35-arbeiten-mit-branches)
	
	3.6 [Matlab und Git](https://gitlab.com/odeegit/odee-gitlab-leitfaden#36-matlab-und-git)
	
	3.7 [Snippets hinzufügen](https://gitlab.com/odeegit/odee-gitlab-leitfaden#37-snippets-hinzuf%C3%BCgen)
	
4. [**Dokumentation hinzufügen**](https://gitlab.com/odeegit/odee-gitlab-leitfaden#4-dokumentation-hinzuf%C3%BCgen)

	4.1 [Markdown language](https://gitlab.com/odeegit/odee-gitlab-leitfaden#41-markdown-language)
	
	4.2 [Wiki](https://gitlab.com/odeegit/odee-gitlab-leitfaden#42-wiki)
	
# 2. Installation

## 2.1 GitLab Account erstellen

Zunächst muss ein Account bei [GitLab](https://gitlab.com/users/sign_in) erstellt werden:

* Bei [GitLab](https://gitlab.com/users/sign_in) unter dem Tab `Register` einen Account erstellen 
* Für die Gruppe [ODEE](https://gitlab.com/lab_odee) Zugriff beantragen

![Request Access](src/img/gitlab/request_access.png)

* Auf Freigabe durch den Administrator warten. Falls keine Freigabe erfolgt, bitte bei den Administratoren melden ([Konrad Koschnick](mailto:konrad.koschnick@stud.h-da.de) oder [Thomas Schatz](mailto:thomas.schatz@stud.h-da.de))

## 2.2 Installation von Git

* Git von der [offiziellen Webseite](https://git-scm.com/downloads) herunterladen
* Setup starten und den Anweisungen folgen

<details>
	<summary> Einzelschritte anzeigen </summary>
	
1. Lizenz "sorgfältig durchlesen" und mit `Next` bestätigen

![Lizenzabkommen](src/img/git/Setup1.png)

2. Zu installierne Komponenten auswählen:

![Komponenten](src/img/git/Setup3.png)

3. Stanardeditor auswählen (nur notwendig, wenn git über die Kommandozeile git-bash bedient wird). Empfehlung: `nano` ist verständlicher als `vim`

![Editor](src/img/git/Setup4_editor.png)

4. Alle restlichen Einstellungen unveränert mit `Next` bestätigen!

![PATH Environment](src/img/git/Setup5.png)
![HTTPS Transport](src/img/git/Setup6.png)
![Line Ending](src/img/git/Setup7.png)
![Terminal Emulator](src/img/git/Setup8.png)
![Extra Options](src/img/git/Setup9.png)
![Experimental Options](src/img/git/Setup10.png)
</details>

# 3. Bedienung
Git kann in der einfachsten Form als eine Cloud angesehen werden (siehe "Codes nur verwenden").
Wird aktiv an Codes gearbeitet, muss man sich mit der Git-Syntax und Funktionsweise vertraut machen. Dies wird im Kapitel danach erläutert.

## 3.1 Codes "nur" verwenden
Jedes Projekt kann geklont (heruntergeladen) werden. Dazu muss lediglich im gewünschten Projekt auf "Download" ![download_btn](src/img/gitlab/download_btn.png) geklickt und die gewünschte Kompression (zip) gewählt werden.
Daraufhin können alle im Repository befindlichen Dateien lokal (ohne git) verwendet und bearbeitet werden. 
**Wichtig:** Wird im remote Repository (auf GitLab) eine Änderung / neue Version veröffentlicht, muss diese wieder manuell heruntergeladen werden. Daher empfiehlt es sich das Repository durch Git überwachen zu lassen, somit werden Änderungen am Programm durch einen **pull** automatisch eingepflegt (Siehe 3.3.1)

## 3.2 Allgemeine Begriffe
In der Git-Umgebung stößt man recht schnell auf wiederkehrende Begriffe. Diese sind allgemeingültig für [Git](https://git-scm.com/), [GitLab](https://about.gitlab.com/), [GitHub](https://github.com/), [GitBucket](https://gitbucket.github.io/), oder andere Remote-Platformen.

* **Repository:** Grundelement im Git - entspricht dem Projekt Ordner. Das Repository (kurz: Repo) enthält alle Projekt-Dateien, Dokumentationen und den Revisionsverlauf.
* **Branch:** Parallelversion eines Repos. Es befindet sich im Repository, beeinflusst aber nicht den "master-branch" (die aktuellste, funktionierende Code-Version). Durch Branches kann man frei am Code arbeiten, ohne an der Master-Version etwas zu verändern. Sind die gewünschten Änderungen vollendet, wird der Branch in den "master"-Branch integriert (merge).
* **Clone:** Lokale Kopie des aktuellen Repos. Alle hier gemachten Änderungen werden von Git verfolgt und können wieder dem remote Repository hinzugefügt werden.
* **Stage:** Geänderte Dateien werden für einen Commit vorbereitet. 
* **Commit:** "Staged" Dateien werden mit einer Kurznachricht im lokalen Repository veröffentlicht.
* **Push:**	Upload zu GitLab (ins remote Repository)
* **Pull:** Download neuerer Dateiversionen aus dem remote Repository.

## 3.3 Ein existierendes Repository klonen & benutzen (mit Git GUI):
Für ein besseres Verständnis empfiehlt es sich im **bash** (Kommandozeilen-Editor) zu arbeiten. Zur täglichen Anwendung ist das **GUI** jedoch schneller.
Hier wird das offizielle Git GUI verwendet. Es gibt allerdings noch zahlreiche GUIs von Drittanbietern für [Windows](https://git-scm.com/download/gui/windows), [Mac](https://git-scm.com/download/gui/mac) und [Linux](https://git-scm.com/download/gui/linux)

### 3.3.1 Repository klonen:

**1. Bei GitLab in gewünschtes Repository navigieren und auf `Clone` klicken und den Link bei `Clone with HTTPS` kopieren**

![gitgui1](src/img/git/gitgui3.png)

**2. Starte Git Gui**

**3. `Clone Existing Repository` auswählen**

![gitgui1](src/img/git/gitgui1.png)

**4. Den Link aus Schritt 1 in `Source Location` eintragen. Bei `Target Directory` den Speicherort auswählen.**

![gitgui2](src/img/git/gitgui2.png)

**5. Mit `Clone` bestätigen und warten.**

**6. Als nächstes öffnet sich das Git GUI des hinzugefügten Repos.**
* Zunächst werden die Benutzerdaten in den Optionen des Repos unter `Edit.. > Options` eingetragen. In der linken Spalte werden die Daten nur für dieses eine Repo gespeichert, rechts gelten sie global für alle neuen Repos.
	
![gitgui5](src/img/git/gitgui5.png)

### 3.3.2 Repository benutzen:

**Die wichtigsten Bedienelemente sind rot markiert.**

* **Rescan:** Sucht nach neuen, modifizierten und gelöschten Dateien im lokalen Repository. Änderungen werden im oberen rechten Fenster (mit dem gelben Balken) angezeigt.
* **Stage Changed:** Alle Dateien werden für den Commit vorbereitet. Alternativ können Dateien einzeln gestaged werden, indem auf das Icon neben dem Dateinamen geklickt wird.
* **Sign Off:** Fügt dem Commit einen Benutzernamen hinzu (wird durch die Einstellung in Schritt 6 automatisch durchgeführt).
* **Commit:** Nimmt alle vorgemerkten Dateien (in Stage Changed) und fügt sie dem **lokalen** Repository hinzu. Hierbei **muss** immer eine aussagekräftige Beschreibung hinzugefügt werden was geändert wurde.
* **Push:** "Schiebt" die Daten in das **remote** Repository (GitLab)

![gitgui4](src/img/git/gitgui4.png)
## 3.4 Ein neues Repository erstellen:

**1. Auf GitLab in der Gruppenansicht in den gewünschten Ordner navigieren**

![gitlab](src/img/gitlab/groups.png)

**2. Neues Projekt erstellen**

![gitlab](src/img/gitlab/new_project.png)

**3. Namen vergeben und `Initialize repository with a README` aktivieren**

![gitlab](src/img/gitlab/blank_project.png)

**4. Den Ablauf von "Ein existierendes Repository klonen" (Schritte 1 bis 6) wiederholen**

**5. Nun können die gewünschten Projekt-Dateien in den lokalen Git-Ordner verschoben werden und ein "initial commit" und "push" durchgeführt werden.**
### 3.5 Arbeiten mit Branches
Branches bieten sich an, wenn ein neues Feature entwickelt wird, was später im Repository vorhanden sein soll. Damit das Hauptprogramm (master-branch) nicht gestört wird, kann eine parallele Version erstellt werden. 
Dies ähnelt einer Baumstruktur - daher "branch". 

* Ein neuer Branch wird im Git GUI über `Branch -> Create` erstellt. 
* Zum Wechseln in einen anderen Branch wird `Branch -> Checkout` ausgewählt
* Der aktuelle Branch wird unter der Befehlsleiste `Current Branch: master` angezeigt.
* Sind die arbeiten im Branch abgeschlossen, kann ein `merge request` in GitLab erstellt werden. Der Administrator überprüft daraufhin die Anfrage und pflegt die Änderungen in den master-branch ein. 

### 3.6 Matlab und Git
Matlab kann direkt mit Git-Repositories kommunizieren. Dazu gibt es [hier](https://blogs.mathworks.com/community/2014/10/20/matlab-and-git/) einen ausführlichen Leitfaden

### Hinzufügen eines Repos:

* Matlab starten und einen neuen Ordner erstellen
* Rechtsklick ins Fenster `Current Folder`
* `Source Control -> Manage Files...` und unter "Source control integration" `Git` auswählen, "Repository path" hinzufügen und mit `Retrieve` bestätigen![manage files](src/img/matlab/manage_files.png)
* Benutzername und Passwort eingeben
* Danach wird das Repository in den zuvor erstellten Ordner geklont

### Benutzung von Git in Matlab
* Wird ein Git-Repository durch Matlab erkannt, erscheint im `Current Folder` automatisch eine Git-spezifische Symbolleiste. ![git status](src/img/matlab/git_status.png)
* Werden Änderungen vorgenommen wechselt das grüne Symbol in ein blaues Viereck.
* Die Bedienelemente werden über die `Source Control` erreicht und entsprechen den üblichen Git-Befehlen ![source control](src/img/matlab/source_control.png)

### 3.7 Snippets hinzufügen
[Snippets](https://docs.gitlab.com/ee/user/snippets.html) sind dazu geeignet, wiederkehrende Programmcodes zu speichern. So können einzelne Funktionen oder häufig genutzte Codes (z.B. GUI-Ordnerauswahl) unabhängig von einem Projekt gespeichert werden.
* Snippets können im `Snippets`-Eintrag in der Menüleiste oder über das `+` hinzugefügt werden. ![snippets](/src/img/gitlab/snippets.png) 

# 4. Dokumentation hinzufügen
Es bietet sich an für jedes Projekt (Repository) eine README-Datei (wie diese Seite) anzulegen.
Diese wird ggf. automatisch angelegt (Ein neues Repository erstellen - Schritt 3) und liegt als README.md (*.md - markdown) vor.
## 4.1 Markdown language
Markdown ist eine vereinfachte Auszeichnugssprache, durch die man mit einer sehr einfachen Syntax überschauliche Textdateien erstellen kann.

[Dieses](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) "Cheat-Sheet" ist eine sehr gute Übersicht über die Syntax.
Prinzipiell kann die Datei in jedem Texteditor erstellt werden. Empfohlen werden kann der Editor [**Notepad++**](https://notepad-plus-plus.org/download/v7.7.1.html) (mit dem [Markdown-Panel Plugin](https://github.com/nea/MarkdownViewerPlusPlus))
Ebenfalls kann man ohne zusätzliche Tools direkt im Browser Markdown-Dateien mit Vorschau erstellen - [StackEdit](https://stackedit.io/)
### Code Beispiele
---
### Überschriften
```
Markdown:
'# Überschrift Ebene 1'
'## Überschrift Ebene 2'
'### Überschrift Ebene 3'
```
Darstellung:
# Überschrift Ebene 1
## Überschrift Ebene 2
### Überschrift Ebene 3

### Listen
```
Markdown:
* Auflistung
1. Nummerierte Liste
```
Darstellung:
* Auflistung
1. Nummerierte Liste

### Code und Blöcke
```
Markdown:
`Hier steht Code`
```
Darstellung:
`Hier steht Code`

Blöcke (mehrzeilige Codes) werden umhüllt von ` ``` ` oder durch Einrücken von 4 Leerzeichen
 
## 4.2 Wiki
Eine ausführlichere Dokumentation kann in Form eines Wikis über den Eintrag in der Seitenleiste erstellt werden.
![wiki](src/img/gitlab/wiki.png)
Die Einträge basieren ebenfalls auf der Markdown-Syntax.

